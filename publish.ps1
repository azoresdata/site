﻿tiddlywiki --output ./public --rendertiddlers [!is[system]] `$:/core/templates/static.tiddler.html static text/plain
tiddlywiki --output ./public --rendertiddler `$:/core/templates/static.template.html static.html text/plain
tiddlywiki --output ./public --rendertiddler `$:/core/templates/static.template.css static/static.css text/plain
tiddlywiki --output ./public --rendertiddler `$:/plugins/dullroar/atomfeed/atom.template.xml atom.xml text/plain
