/*\
title: checkurl
type: application/javascript
module-type: macro
\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

exports.name = "checkurl";

exports.params = [];

/*
Run the macro
*/
exports.run = function() {
    if (location.hostname === "localhost" || location.hostname === "127.0.0.1"){
    return "No";
}
else{ return "Yes"};
};

})();